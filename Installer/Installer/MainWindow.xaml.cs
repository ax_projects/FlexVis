﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Installer.ViewModels;
using MessageBox = System.Windows.MessageBox;

namespace Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string BrowseFolderSuffix = @"\FlexVisClient\";

        private readonly MainWindowViewModel _vm = null;

        public MainWindow()
        {
            InitializeComponent();

            _vm = DataContext as MainWindowViewModel;
            FtpPasswordBox.Password = _vm?.FtpPassword ?? string.Empty;
        }

        private void OnBlowseClicked(object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    _vm.InstallFolder = dialog.SelectedPath + BrowseFolderSuffix;
                }
            }
        }

        private void OnStartClicked(object sender, RoutedEventArgs e)
        {
            if (_vm.Install())
            {
                MessageBox.Show("Installation was finished. Installer will be closed automatically", "Info",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Installation was not finished successfully. Program will removed", "Installation Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                _vm.Uninstall();
            }
            Close();
        }

        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (_vm != null)
            {
                _vm.FtpPassword = FtpPasswordBox.Password;
            }
        }
    }
}
