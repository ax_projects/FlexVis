﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Security;
using System.Text.RegularExpressions;
using System.Windows;
using System.Xml.Linq;
using Ionic.Zip;
using IWshRuntimeLibrary;

namespace Installer.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged implementation

        /// <summary>
        /// Call to change property
        /// </summary>
        /// <typeparam name="T">Property type</typeparam>
        /// <param name="value">New property value</param>
        /// <param name="maskedValue">Field with old property value (masked field)</param>
        /// <param name="propertyName">Name of property</param>
        protected void ChangeProperty<T>(ref T value, ref T maskedValue, string propertyName)
        {
            bool isChanged;
            if (maskedValue == null)
            {
                isChanged = (value != null);
            }
            else
            {
                isChanged = !maskedValue.Equals(value);
            }
            maskedValue = value;
            if (isChanged)
            {
                OnPropertyChanged(propertyName);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Constants

        private const string DefaultInstallFolder = @"C:\FlexVisClient\";
        private const string UpdateZip = @"update.zip";
        private const string ConfigZip = @"config.zip";

        private const string PatchingFile = @"update.xml";
        private const string FtpAddressTag = @"BaseUri";
        private const string FtpLoginTag = @"RemoteLogin";
        private const string FtpPasswordTag = @"RemotePassword";

        private const string LinkIconLocationFile = @"FlexVisClient.exe, 0";
        private const string LinkDescription = @"FlexVisClient by AXONIM";
        private const string LinkWay = @"UpdateProgram.exe";
        private const string LinkName = @"FlexVisClient";

        private const string AppConfigWay = @"FlexVisClient.exe.config";

        /// <summary>
        /// key = key name
        /// value = property name
        /// </summary>
        private readonly Dictionary<string, string> _replaceAppConfigs = new Dictionary<string, string>
        {
            {"DBHost", nameof(DbHost)},
            {"DBName", nameof(DbName)},
            {"MongoHost", nameof(MongoHost)},
            {"MongoDBName", nameof(MongoDbName)},
            {"ReportServer", nameof(ReportServer)},
            {"ClientPort", nameof(ClientPort)},
            {"ServerIp", nameof(ServerIp)},
            {"ServerPort", nameof(ServerPort)},
            {"ClientLocalIP", nameof(ClientLocalIp)},
            {"UserName", nameof(UserName)},
            {"Password", nameof(Password)},
        };

        #endregion

        #region Masked fields

        private string _maskInstallFolder = string.Empty;
        private string _maskFtpAddress = string.Empty;
        private string _maskFtpLogin = string.Empty;
        private string _maskFtpPassword = string.Empty;
        private bool _maskIsCanStart = false;
        private bool _maskIsEnabled = true;
        private string _maskDbHost = string.Empty;
        private string _maskDbName = string.Empty;
        private string _maskMongoHost = string.Empty;
        private string _maskMongoDbName = string.Empty;
        private string _maskReportServer = string.Empty;
        private string _maskClientLocalIp = string.Empty;
        private string _maskClientPort = string.Empty;
        private string _maskServerIp = string.Empty;
        private string _maskServerPort = string.Empty;

        #endregion

        #region Properties

        public string InstallFolder
        {
            get => _maskInstallFolder;
            set => SetInstallPath(value);
        }

        public string FtpAddress
        {
            get => _maskFtpAddress;
            set
            {
                string ftp = value;
                if (!ftp.EndsWith("/"))
                {
                    ftp += "/";
                }
                ChangeProperty(ref ftp, ref _maskFtpAddress, nameof(FtpAddress));
            }
        }

        public string FtpLogin
        {
            get => _maskFtpLogin;
            set => ChangeProperty(ref value, ref _maskFtpLogin, nameof(FtpLogin));
        }

        public string FtpPassword
        {
            get => _maskFtpPassword;
            set => ChangeProperty(ref value, ref _maskFtpPassword, nameof(FtpPassword));
        }

        public bool IsCanStart
        {
            get => _maskIsCanStart;
            private set => ChangeProperty(ref value, ref _maskIsCanStart, nameof(IsCanStart));
        }

        public bool IsEnabled
        {
            get => _maskIsEnabled;
            private set => ChangeProperty(ref value, ref _maskIsEnabled, nameof(IsEnabled));
        }

        public string DbHost
        {
            get => _maskDbHost;
            set => ChangeProperty(ref value, ref _maskDbHost, nameof(DbHost));
        }

        public string DbName
        {
            get => _maskDbName;
            set => ChangeProperty(ref value, ref _maskDbName, nameof(DbName));
        }

        public string MongoHost
        {
            get => _maskMongoHost;
            set => ChangeProperty(ref value, ref _maskMongoHost, nameof(MongoHost));
        }

        public string MongoDbName
        {
            get => _maskMongoDbName;
            set => ChangeProperty(ref value, ref _maskMongoDbName, nameof(MongoDbName));
        }

        public string ReportServer
        {
            get => _maskReportServer;
            set => ChangeProperty(ref value, ref _maskReportServer, nameof(ReportServer));
        }
        public string ClientLocalIp
        {
            get => _maskClientLocalIp;
            set => ChangeProperty(ref value, ref _maskClientLocalIp, nameof(ClientLocalIp));
        }
        public string ClientPort
        {
            get => _maskClientPort;
            set => ChangeProperty(ref value, ref _maskClientPort, nameof(ClientPort));
        }

        public string ServerIp
        {
            get => _maskServerIp;
            set => ChangeProperty(ref value, ref _maskServerIp, nameof(ServerIp));
        }

        public string ServerPort
        {
            get => _maskServerPort;
            set => ChangeProperty(ref value, ref _maskServerPort, nameof(ServerPort));
        }

        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;

        #endregion

        public MainWindowViewModel()
        {
            SetInstallPath(DefaultInstallFolder);
            FtpAddress = "ftp://10.8.1.21/FlexVisClient/";
            FtpLogin = "flexvis_update";
            FtpPassword = "FlexVis@TP";
            DbHost = "localhost";
            DbName = "gas";
            MongoHost = "localhost";
            MongoDbName = "GasHist";
            ReportServer = "localhost:8080";
            ClientLocalIp = NetworkInterface.GetIsNetworkAvailable() ? GetLocalIpAddress() : string.Empty;
            ClientPort = "8101";
            ServerIp = string.Empty;
            ServerPort = "8000";
        }

        #region Methods

        /// <summary>
        /// Set install path
        /// </summary>
        /// <param name="path">Path to set</param>
        private void SetInstallPath(string path)
        {
            const string doubleBackSlash = @"\\";
            const string backSlash = @"\";

            //calculate new install path value
            string newPath = path.TrimEnd('\\') + "\\";
            while (newPath.Contains(doubleBackSlash))
            {
                newPath = newPath.Replace(doubleBackSlash, backSlash);
            }
            
            _maskInstallFolder = newPath;
            OnPropertyChanged(nameof(InstallFolder));

            DirectoryInfo pathInfo = new DirectoryInfo(newPath);
            IsCanStart = !pathInfo.Exists || !Directory.EnumerateFileSystemEntries(newPath).Any();
            if (IsCanStart)
            {
                return;
            }

            MessageBoxResult userAnswer =
                MessageBox.Show("Folder is already exists. Do you want to rewrite all data?",
                    "Folder already not empty", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (userAnswer != MessageBoxResult.Yes)
            {
                return;
            }

            do
            {
                MessageBox.Show($"Save info that you need from way '{InstallFolder}' and then press \"OK\"",
                    "Save info that you need", MessageBoxButton.OK, MessageBoxImage.Information);

                userAnswer =
                    MessageBox.Show($"Are you really save all you need from folder '{InstallFolder}'?",
                        "Folder already not empty", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

                if (userAnswer == MessageBoxResult.Cancel)
                {
                    MessageBox.Show("Rewriting was canceled",
                        "Rewriting was canceled", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            } while (userAnswer != MessageBoxResult.Yes);

            Uninstall();

            MessageBox.Show("Old program version was removed", "Old program version was removed", MessageBoxButton.OK, MessageBoxImage.Information);

            IsCanStart = true;
        }

        /// <summary>
        /// Install program
        /// </summary>
        /// <returns>true if all is good, else false</returns>
        public bool Install()
        {
            IsEnabled = false;

            try
            {
                if (!UnzipFile(UpdateZip, InstallFolder))
                {
                    throw new Exception($"Cannot extract {UpdateZip} to {InstallFolder}");
                }
                if (!UnzipFile(ConfigZip, InstallFolder))
                {
                    throw new Exception($"Cannot extract {ConfigZip} to {InstallFolder}");
                }
                if (!PatchConfig())
                {
                    throw new Exception("Cannot patch config files");
                }

                CreateShortcut(LinkName, Environment.GetFolderPath(Environment.SpecialFolder.Desktop), InstallFolder,
                    LinkWay);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                IsEnabled = true;
            }
        }

        /// <summary>
        /// Uninstaller
        /// Delete folder with program
        /// </summary>
        public void Uninstall()
        {
            IsEnabled = false;

            try
            {
                //Directory.Delete(InstallFolder, true);
                MessageBox.Show("Old program version was not removed. Please, remove it manually", "Old program version was not removed", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch
            {
                // ignored because uninstalling
            }
            finally
            {
                IsEnabled = true;
            }
        }

        /// <summary>
        /// Unzip file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="targetPath">Path to extract archieve</param>
        /// <returns>true if all is good, else false</returns>
        private bool UnzipFile(string filePath, string targetPath)
        {
            // Unzip
            if (!Regex.IsMatch(filePath, @"\.zip")) return false;

            using (ZipFile zip = ZipFile.Read(filePath))
            {
                zip.ExtractAll(targetPath, ExtractExistingFileAction.OverwriteSilently);
            }
            return true;
        }

        private bool PatchConfig()
        {
            string filePath = InstallFolder + PatchingFile;
            string programConfigFilePath = InstallFolder + AppConfigWay;
            bool isTagsReplaced = ReplaceXmlTag(filePath, FtpAddressTag, FtpAddress) &&
                                  ReplaceXmlTag(filePath, FtpLoginTag, FtpLogin) &&
                                  ReplaceXmlTag(filePath, FtpPasswordTag, FtpPassword);

            bool isAttributesReplaced = _replaceAppConfigs.Aggregate(true,
                (b, pair) => b && ReplaceXmlAttribute(programConfigFilePath, pair.Key,
                                 GetType().GetProperty(pair.Value)?.GetValue(this, null).ToString() ?? string.Empty));
            return isTagsReplaced && isAttributesReplaced;
        }

        /// <summary>
        /// Replace tag in xml
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="tagName"></param>
        /// <param name="newValue"></param>
        /// <returns>true, if all was successful, else false</returns>
        private bool ReplaceXmlTag(string filePath, string tagName, string newValue)
        {
            try
            {
                XDocument xml = XDocument.Load(filePath);

                var elementsToUpdate = xml.Descendants()
                    .Where(o => o.Name == tagName && !o.HasElements);

                //update elements value
                foreach (XElement element in elementsToUpdate)
                {
                    element.Value = newValue;
                }

                xml.Save(filePath);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot replace tag" + e.Message);
            }
            return false;
        }

        private bool ReplaceXmlAttribute(string filePath, string keyAttributeValue, string newValue)
        {
            try
            {
                XDocument xml = XDocument.Load(filePath);

                var elementsToUpdate = xml.Descendants()
                    .Where(o => o.Name == "add" &&
                                o.HasAttributes)
                    .Where(o =>
                        o.Attributes("key").Aggregate(false,
                            (b, attribute) => attribute.Value == keyAttributeValue));

                //update elements value
                foreach (XElement element in elementsToUpdate)
                {
                    foreach (XAttribute attribute in element.Attributes("value"))
                    {
                        attribute.Value = newValue;
                    }
                }

                xml.Save(filePath);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot replace attribute" + e.Message);
            }
            return false;
        }

        /// <summary>
        /// Create shortcut method
        /// </summary>
        /// <param name="shortcutName">Display name</param>
        /// <param name="shortcutPath">Shortcut path</param>
        /// <param name="workingDir">Program directory (usually)</param>
        /// <param name="targetFileRelativePath">File to shortcut relative path (depends on <see cref="workingDir"/> param)</param>
        public static void CreateShortcut(string shortcutName, string shortcutPath, string workingDir,
            string targetFileRelativePath)
        {
            string shortcutLocation = Path.Combine(shortcutPath, shortcutName + ".lnk");
            int i = 0;

            while (new FileInfo(shortcutLocation).Exists)
            {
                shortcutLocation = Path.Combine(shortcutPath, shortcutName + $" ({i}).lnk");
                i++;
            }

            WshShell shell = new WshShell();
            IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcut.WorkingDirectory = workingDir;
            shortcut.Description = LinkDescription;
            shortcut.IconLocation = workingDir + LinkIconLocationFile;
            shortcut.TargetPath = workingDir + targetFileRelativePath;
            // Save the shortcut
            shortcut.Save();
        }

        public static string GetLocalIpAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        #endregion
    }
}