@echo off

set configFolder=config
set updateCreationFolder=update
set resultFolder=result

echo Cleanup previous results
rmdir /s /q "%resultFolder%"

echo Init
call init.bat

echo Make config
mkdir "%configFolder%"
move "program\FlexVisClient.exe.config" "%configFolder%"
cd "%configFolder%"
7z a -tzip "..\%configFolder%.zip" "*"
cd ..

echo Make update
mkdir "%updateCreationFolder%"
copy "program\*" "%updateCreationFolder%"
copy "%configFolder%\FlexVisClient.exe.config" "program\"
copy "updater\*" "%updateCreationFolder%"
cd "%updateCreationFolder%"
7z a -tzip "..\%updateCreationFolder%.zip" "*"
cd ..

echo Make installer
copy "%updateCreationFolder%.zip" "installer\"
copy "%configFolder%.zip" "installer\"
cd "installer"
7z a -t7z "..\sfx\installer.7z" "*"
cd "../sfx"
copy /b "7zS.sfx" + "config.txt" + "installer.7z" "..\installer.exe"
del "installer.7z"
cd ..
del "installer\%updateCreationFolder%.zip"
del "installer\%configFolder%.zip"

echo Build results
mkdir "%resultFolder%"
copy "updater\update.xml" "%resultFolder%\"
copy "%updateCreationFolder%.zip" "%resultFolder%\FlexVisClient-update.zip"
copy "installer.exe" "%resultFolder%\FlexVisClient-install.exe"

echo Cleanup
del "installer.exe"
del "%configFolder%.zip"
del "%updateCreationFolder%.zip"
rmdir /s /q "%configFolder%"
rmdir /s /q "%updateCreationFolder%"

rmdir /s /q "installer"
rmdir /s /q "updater"
