﻿$xmlFile = "updater\update.xml"
$exeFile = "program\FlexVisClient.exe"

#Function from https://gist.github.com/jpoehls/2726969
function Edit-XmlNodes {
param (
    [xml] $doc = $(throw "doc is a required parameter"),
    [string] $xpath = $(throw "xpath is a required parameter"),
    [string] $value = $(throw "value is a required parameter"),
    [bool] $condition = $true
)    
    if ($condition -eq $true) {
        $nodes = $doc.SelectNodes($xpath)
            
        foreach ($node in $nodes) {
            if ($node -ne $null) {
                if ($node.NodeType -eq "Element") {
                    $node.InnerXml = $value
                }
                else {
                    $node.Value = $value
                }
            }
        }
    }
}

#Function from https://gallery.technet.microsoft.com/scriptcenter/How-to-remove-UTF8-Byte-a66fc0b2
Function Remove-UTF8BOM
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [String] $FilePath
    )
    Try  
    {
        [System.IO.FileInfo] $file = Get-Item -Path $FilePath
        $sequenceBOM = New-Object System.Byte[] 3
        $reader = $file.OpenRead()
        $bytesRead = $reader.Read($sequenceBOM, 0, 3)
        $reader.Dispose()
        #A UTF-8+BOM string will start with the three following bytes. Hex: 0xEF0xBB0xBF, Decimal: 239 187 191
        if ($bytesRead -eq 3 -and $sequenceBOM[0] -eq 239 -and $sequenceBOM[1] -eq 187 -and $sequenceBOM[2] -eq 191)
        {
            $utf8NoBomEncoding = New-Object System.Text.UTF8Encoding($False)
            [System.IO.File]::WriteAllLines($FilePath, (Get-Content $FilePath), $utf8NoBomEncoding)
            Write-Host "Remove UTF-8 BOM successfully"
        }
        Else
        {
            Write-Warning "Not UTF-8 BOM file"
        }    
    }
    Catch [Exception] 
    { 
        Write-Error $_.Exception.ToString() 
    }  
}


Write-Output "Get version of $exeFile"

$version = (Get-Item $exeFile).VersionInfo.FileVersion

Write-Output "Version is $version"
Write-Output "Patching $xmlFile"

$xml = [xml](Get-Content $xmlFile)

Edit-XmlNodes $xml -xpath "/Manifest/@version" -value "$version"

$xml.save($xmlFile)

Remove-UTF8BOM($xmlFile)
