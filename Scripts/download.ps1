﻿$apiUrl = 'https://ci.appveyor.com/api'
$token = 'uu43pefadngej919a5gn'
$headers = @{
    "Authorization" = "Bearer $token"
    "Content-type"  = "application/json"
}
$accountName = 'AJIOB'
$projectSlug = 'flexvis'
$projectBranch = 'master'

$downloadLocation = 'downloaded'

# get project with last build details
$project = Invoke-RestMethod -Method Get -Uri "$apiUrl/projects/$accountName/$projectSlug/branch/$projectBranch" -Headers $headers

# we assume here that build has a single job
# get this job id
foreach ($job in $project.build.jobs) {
    $jobId = $job.jobId

    # get job artifacts (just to see what we've got)
    $artifacts = Invoke-RestMethod -Method Get -Uri "$apiUrl/buildjobs/$jobId/artifacts" -Headers $headers

    # here we just take the first artifact, but you could specify its file name
    # $artifactFileName = 'MyWebApp.zip'
    foreach ($artifact in $artifacts) {
        $artifactFileName = $artifact.fileName

        Write-Output "Downloading $artifactFileName"

        # artifact will be downloaded as
        $localArtifactPath = "$downloadLocation\$artifactFileName"

        # download artifact
        # -OutFile - is local file name where artifact will be downloaded into
        # the Headers in this call should only contain the bearer token, and no Content-type, otherwise it will fail!
        Invoke-RestMethod -Method Get -Uri "$apiUrl/buildjobs/$jobId/artifacts/$artifactFileName" `
            -OutFile $localArtifactPath -Headers @{ "Authorization" = "Bearer $token" }
    }
}