@echo off

set downloadFolder=downloaded
set installFolder=installer
set updateFolder=updater

echo Cleanup previous
rmdir /s /q "%installFolder%"
rmdir /s /q "%updateFolder%"
rmdir /s /q "%downloadFolder%"

echo Download packages
mkdir "%downloadFolder%"
powershell -executionpolicy Unrestricted -File download.ps1

echo Unpack installer
mkdir "%installFolder%"
7z x "%downloadFolder%\Installer.zip" -o"%installFolder%" * -r

echo Unpack updater
mkdir "%updateFolder%"
7z x "%downloadFolder%\Updater.zip" -o"%updateFolder%" * -r

echo Patch update version
powershell -executionpolicy Unrestricted -File patch_version.ps1

echo Cleanup
rmdir /s /q "%downloadFolder%"
