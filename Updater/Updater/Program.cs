﻿using System;
using System.Data;
using System.Diagnostics;
using RedCell.Diagnostics.Update;

namespace UpdateProgram
{
    public class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 0 && args[0] == Updater.UpdaterSecondPartParam)
            {
                Updater.UpdatePart2();
                return;
            }

            Console.WriteLine("Check for application new version");

            Manifest config = null;

            try
            {
                //finalizing previous updating
                Updater.UpdatePart3();

                config = Updater.Load(Updater.DefaultConfigFile);
                if (config == null)
                {
                    throw new NoNullAllowedException();
                }

                Updater updater = new Updater(config);
                Log.Console = true;
                if (updater.Check())
                {
                    return;
                }

                Console.WriteLine("Starting the application");

                Process.Start(config.AppWay);

                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("Cannot check for application new version or start application. Press any key to exit");
            Console.ReadKey();
        }
    }
}
