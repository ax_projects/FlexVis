﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace RedCell.Net
{
    /// <summary>
    /// Fetches web pages.
    /// </summary>
    public class Fetch
    {
        #region Initialiation
        /// <summary>
        /// Initializes a new instance of the <see cref="Fetch"/> class.
        /// </summary>
        public Fetch()
        {
            Retries = 5;
            Timeout = 60000;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the response.
        /// </summary>
        public FtpWebResponse Response { get; private set; }

        /// <summary>
        /// Gets the response data.
        /// </summary>
        public byte[] ResponseData { get; private set; }

        /// <summary>
        /// Gets or sets the retries.
        /// </summary>
        /// <value>The retries.</value>
        public int Retries { get; set; }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        /// <value>The timeout.</value>
        public int Timeout { get; set; }

        /// <summary>
        /// Gets or sets the retry sleep in milliseconds.
        /// </summary>
        /// <value>The retry sleep.</value>
        public int RetrySleep { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Fetch"/> is success.
        /// </summary>
        /// <value><c>true</c> if success; otherwise, <c>false</c>.</value>
        public bool Success { get; private set; }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the specified URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="login">Login to auth</param>
        /// <param name="password">Password to auth</param>
        public void Load(string url, string login, string password)
        {
            for (int retry = 0; retry < Retries; retry++)
            {
                try
                {
                    // Get the object used to communicate with the server.  
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                    request.Method = WebRequestMethods.Ftp.DownloadFile;

                    // This example assumes the FTP site uses anonymous logon.  
                    request.Credentials = new NetworkCredential(login, password);

                    Response = (FtpWebResponse)request.GetResponse();

                    using (Stream stream = Response.GetResponseStream())
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            if (stream != null)
                                for (int b; (b = stream.ReadByte()) != -1;)
                                    ms.WriteByte((byte) b);
                            ResponseData = ms.ToArray();
                        }

                    }

                    Console.WriteLine("Download Complete, status {0}", Response.StatusDescription);

                    Success = true;
                    break;
                }
                catch (WebException ex)
                {
                    Console.WriteLine(":Exception " + ex.Message);
                    Response = ex.Response as FtpWebResponse;
                    if (ex.Status == WebExceptionStatus.Timeout)
                    {
                        Thread.Sleep(RetrySleep);
                        continue;
                    }
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(":Exception " + e.Message);
                }
            }
        }

        /// <summary>
        /// Gets the specified URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="login">Login to auth</param>
        /// <param name="password">Password to auth</param>
        /// <returns></returns>
        public static byte[] Get(string url, string login, string password)
        {
            var f = new Fetch();
            f.Load(url, login, password);
            return f.ResponseData;
        }
        #endregion
    }
}
