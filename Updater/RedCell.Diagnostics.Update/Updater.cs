﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Ionic.Zip;
using RedCell.Net;

namespace RedCell.Diagnostics.Update
{
    public class Updater
    {
        #region Constants

        /// <summary>
        /// The default configuration file
        /// </summary>
        public const string DefaultConfigFile = "update.xml";

        public const string WorkPath = "update";
        public const string UpdaterSecondPartParam = "update_part2";
        #endregion

        #region Fields
        private volatile bool _updating;
        private readonly Manifest _localConfig;
        private Manifest _remoteConfig;

        #endregion

        #region Initialization

        /// <summary>
        /// Initializes a new instance of the <see cref="Updater"/> class.
        /// </summary>
        /// <param name="manifest">Local configuration</param>
        public Updater(Manifest manifest)
        {
            _localConfig = manifest;
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Updater"/> class.
        /// </summary>
        /// <param name="configFileWay">The configuration file way</param>
        public static Manifest Load(string configFileWay)
        {
            Log.Debug = true;

            FileInfo configFile = new FileInfo(configFileWay);

            Log.Write("Loaded.");
            Log.Write("Initializing using file '{0}'.", configFile.FullName);
            if (!configFile.Exists)
            {
                Log.Write("Config file '{0}' does not exist, stopping.", configFile.Name);
                return null;
            }

            string data = File.ReadAllText(configFile.FullName);
            return new Manifest(data);
        }

        #region Methods

        /// <summary>
        /// Checks the specified state.
        /// </summary>
        /// <returns>true, if application was updated, else false</returns>
        public bool Check()
        {
            Log.Write("Check starting.");

            if (_updating)
            {
                Log.Write("Updater is already updating.");
                Log.Write("Check ending.");
                return false;
            }
            _updating = true;
            var remoteUri = new Uri(_localConfig.BaseUri + _localConfig.UpdateMetadata);

            Log.Write("Fetching '{0}'.", remoteUri.AbsoluteUri);
            var http = new Fetch { Retries = 5, RetrySleep = 30000, Timeout = 30000 };
            http.Load(remoteUri.AbsoluteUri, _localConfig.RemoteLogin, _localConfig.RemotePassword);
            if (!http.Success)
            {
                Log.Write("Fetch error: {0}", http.Response.StatusDescription);
                this._remoteConfig = null;
                return false;
            }

            string data = Encoding.UTF8.GetString(http.ResponseData);
            this._remoteConfig = new Manifest(data);

            if (this._remoteConfig == null)
            {
                Log.Write("Cannot read remote config");
                return false;
            }

            if (this._localConfig.SecurityToken != this._remoteConfig.SecurityToken)
            {
                Log.Write("Security token mismatch.");
                return false;
            }
            Log.Write("Remote config is valid.");
            Log.Write("Local version is  {0}.", this._localConfig.Version);
            Log.Write("Remote version is {0}.", this._remoteConfig.Version);

            if (this._remoteConfig.Version == this._localConfig.Version)
            {
                Log.Write("Versions are the same.");
                Log.Write("Check ending.");
                return false;
            }
            if (this._remoteConfig.Version < this._localConfig.Version)
            {
                Log.Write("Remote version is older. That's weird.");
                Log.Write("Check ending.");
                return false;
            }

            Log.Write("Remote version is newer. Updating.");
            bool res = Update();
            _updating = false;
            Log.Write("Check ending.");
            if (res != true)
                throw new Exception("Cannot update application");

            return true;
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        private bool Update()
        {
            Log.Write("Updating '{0}' files.", this._remoteConfig.Payloads.Length);

            // Clean up failed attempts.
            if (Directory.Exists(WorkPath))
            {
                Log.Write("WARNING: Update directory already exists.");
                try { Directory.Delete(WorkPath, true); }
                catch (IOException)
                {
                    Log.Write("Cannot delete open directory '{0}'.", WorkPath);
                    return false;
                }
            }

            Directory.CreateDirectory(WorkPath);

            // Download files in manifest.
            foreach (string update in this._remoteConfig.Payloads)
            {
                Log.Write("Fetching '{0}'.", update);
                var url = this._remoteConfig.BaseUri + update;
                var file = Fetch.Get(url, _localConfig.RemoteLogin, _localConfig.RemotePassword);
                if (file == null)
                {
                    Log.Write("Fetch failed.");
                    return false;
                }
                var info = new FileInfo(Path.Combine(WorkPath, update));
                Directory.CreateDirectory(info.DirectoryName);
                File.WriteAllBytes(Path.Combine(WorkPath, update), file);

                // Unzip
                if (Regex.IsMatch(update, @"\.zip"))
                {
                    try
                    {
                        var zipfile = Path.Combine(WorkPath, update);
                        using (var zip = ZipFile.Read(zipfile))
                            zip.ExtractAll(WorkPath, ExtractExistingFileAction.Throw);
                        File.Delete(zipfile);
                    }
                    catch (Exception ex)
                    {
                        Log.Write("Unpack failed: {0}", ex.Message);
                        return false;
                    }
                }
            }

            // Restart.
            string myPath = Process.GetCurrentProcess().MainModule.FileName;
            Process.Start(WorkPath + "\\" + (new FileInfo(myPath).Name), UpdaterSecondPartParam);

            return true;
        }

        /// <summary>
        /// Continue installing update
        /// </summary>
        public static void UpdatePart2()
        {
            PatchVersion();

            //block of locking (wait while unlocking)
            while (true)
            {
                try
                {
                    // Copy everything.
                    var directory = new DirectoryInfo(WorkPath);
                    var files = directory.GetFiles("*.*", SearchOption.AllDirectories);
                    foreach (FileInfo file in files)
                    {
                        string destination = file.FullName.Replace(directory.FullName + @"\", "");
                        Log.Write("installing file '{0}'.", destination);
                        Directory.CreateDirectory(new FileInfo(destination).DirectoryName ?? throw new InvalidOperationException("Cannot load destination folder"));
                        file.CopyTo(destination, true);
                    }
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            // Restart.
            string myPath = Process.GetCurrentProcess().MainModule.FileName;
            FileInfo myPathInfo = new FileInfo(myPath);
            Process.Start(myPathInfo.DirectoryName + "\\..\\" + (myPathInfo.Name));
        }

        /// <summary>
        /// Finalize installing update
        /// </summary>
        public static void UpdatePart3()
        {
            // Clean up.
            Log.Write("Deleting work directory.");
            try { Directory.Delete(WorkPath, true); }
            catch (IOException)
            {
                Log.Write("Directory not exists or locked '{0}'.", WorkPath);
            }
        }

        private static void PatchVersion()
        {
            Manifest newManifest = Load(DefaultConfigFile);

            ReplaceXmlAttribute(@"..\" + DefaultConfigFile, DefaultConfigFile, Manifest.VersionAttributeName, newManifest.Version.ToString());
        }

        /// <summary>
        /// Replace xml attribute value
        /// </summary>
        /// <param name="inputFilePath">Path to input file</param>
        /// <param name="outputFilePath">Path to save patched file</param>
        /// <param name="attributeName">Attribute name</param>
        /// <param name="newValue">New attibute value</param>
        /// <returns></returns>
        private static bool ReplaceXmlAttribute(string inputFilePath, string outputFilePath, string attributeName, string newValue)
        {
            try
            {
                XDocument xml = XDocument.Load(inputFilePath);

                IEnumerable<XAttribute> elementsToUpdate = xml.Descendants()
                    .Where(o => o.HasAttributes)
                    .SelectMany(o =>
                        o.Attributes(attributeName));

                //update elements value
                foreach (XAttribute attribute in elementsToUpdate)
                {
                    attribute.Value = newValue;
                }

                xml.Save(outputFilePath);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot replace attribute" + e.Message);
            }
            return false;
        }

        #endregion
    }
}
